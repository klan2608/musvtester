#ifndef MAINWINDOW_H
#define MAINWINDOW_H

//#define DEBUG

#include <QMainWindow>
#ifdef SERIAL_ON
#include <QtSerialPort/QSerialPort>
#endif
#include "protocol.h"

namespace Ui {
class MainWindow;
}

class SettingsDialog;
class protocol;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:
    void openSerialPort();
    void closeSerialPort();
    void about();
    void writeData(const QByteArray &data);
    void readData();

    void on_btnRollKpSet_clicked();
    void on_btnRollLpSet_clicked();
    void on_btnRollKiSet_clicked();
    void on_btnRollLiSet_clicked();
    void on_btnRollKdSet_clicked();
    void on_btnRollLdSet_clicked();
    void on_btnRollLSet_clicked();
    void on_btnSetPIDRoll_clicked();
    void on_btnReadPIDRoll_clicked();
    void on_btnPitchKpSet_clicked();
    void on_btnPitchLpSet_clicked();
    void on_btnPitchKiSet_clicked();
    void on_btnPitchLiSet_clicked();
    void on_btnPitchKdSet_clicked();
    void on_btnPitchLdSet_clicked();
    void on_btnPitchLSet_clicked();
    void on_btnSetPIDPitch_clicked();
    void on_btnReadPIDPitch_clicked();
    void on_btnYawKpSet_clicked();
    void on_btnYawLpSet_clicked();
    void on_btnYawKiSet_clicked();
    void on_btnYawLiSet_clicked();
    void on_btnYawKdSet_clicked();
    void on_btnYawLdSet_clicked();
    void on_btnYawLSet_clicked();
    void on_btnSetPIDYaw_clicked();
    void on_btnReadPIDYaw_clicked();
    void on_sldRollOut_valueChanged(int value);
    void on_sldPitchOut_valueChanged(int value);
    void on_sldYawOut_valueChanged(int value);
    void on_btnMagCalibReadData_clicked();
    void on_btnMagCalibOn_clicked();
    void on_btnMagCalibOff_clicked();
    void on_btnZoomInc_clicked();
    void on_btnZoomDec_clicked();
    void on_btnColorInc_clicked();
    void on_btnColorDec_clicked();
    void on_btbCameraTV_clicked();
    void on_btbCameraIR_clicked();
    void on_btbCameraPhoto_clicked();
    void on_btnFocus_clicked();
    void on_btnTakePhoto_clicked();
    void on_btnBurstClear_clicked();
    void on_btnBurst_clicked();

    void on_btnStabilizationOn_clicked();

    void on_btnStabilizationOff_clicked();

    void on_btnMotorOn_clicked();

    void on_btnMotorOff_clicked();

    void on_btnParkingOn_clicked();

    void on_btnParkingOff_clicked();

    void on_btnSetZero_clicked();

    void on_btnBurstStop_clicked();

private:
    int iter;
#ifdef DEBUG
    QByteArray debugArray;
#endif
    u_int8_t zoom;
    u_int8_t color;
    Ui::MainWindow *ui;
    SettingsDialog *settings;
#ifdef SERIAL_ON
    QSerialPort *serial;
#endif
    protocolName::protocol protocolMusv;
    void initActionsConnections();
    void UpdateAngles();
    void SetZoom(u_int8_t value);
    void SetColor(u_int8_t value);
    void UpdateLayble();
    void ClearCalibData();
};

#endif // MAINWINDOW_H
