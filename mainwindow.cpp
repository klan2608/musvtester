#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "settingsdialog.h"
#include "QDebug"
#include "QMessageBox"
#ifdef SERIAL_ON
#include <QtSerialPort/QSerialPort>
#endif
using namespace protocolName;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

#ifdef SERIAL_ON
    serial = new QSerialPort();
#endif
    settings = new SettingsDialog;
    iter = 0;
    zoom = 1;
    color = 1;
    SetZoom(zoom);
    SetColor(color);

    ClearCalibData();

    ui->btnParkingOff->setEnabled(false);
    ui->lblParkingName->setVisible(false);


    ui->actionConnect->setEnabled(true);
    ui->actionDisconnect->setEnabled(false);
    ui->actionQuit->setEnabled(true);
    ui->actionConfigure->setEnabled(true);

    initActionsConnections();
#ifdef SERIAL_ON
    connect(serial, SIGNAL(error(QSerialPort::SerialPortError)), this,
            SLOT(handleError(QSerialPort::SerialPortError)));

    connect(serial, SIGNAL(readyRead()), this, SLOT(readData()));
#endif

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::openSerialPort()
{
    SettingsDialog::Settings p = settings->settings();
#ifdef SERIAL_ON
    serial->setPortName(p.name);
    if (serial->open(QIODevice::ReadWrite)) {
        serial->setBaudRate(p.baudRate);
        serial->setDataBits(p.dataBits);
        serial->setParity(p.parity);
        serial->setStopBits(p.stopBits);
        serial->setFlowControl(p.flowControl);
//            console->setEnabled(true);
//            console->setLocalEchoEnabled(p.localEchoEnabled);
            ui->actionConnect->setEnabled(false);
            ui->actionDisconnect->setEnabled(true);
            ui->actionConfigure->setEnabled(false);
            ui->statusBar->showMessage(tr("Connected to %1 : %2, %3, %4, %5, %6")
                                       .arg(p.name).arg(p.stringBaudRate).arg(p.stringDataBits)
                                       .arg(p.stringParity).arg(p.stringStopBits).arg(p.stringFlowControl));
    } else {
        QMessageBox::critical(this, tr("Error"), serial->errorString());

        ui->statusBar->showMessage(tr("Open error"));
    }
#endif
}

void MainWindow::closeSerialPort()
{
#ifdef SERIAL_ON
    if (serial->isOpen())
        serial->close();
//    console->setEnabled(false);
#endif
    ui->actionConnect->setEnabled(true);
    ui->actionDisconnect->setEnabled(false);
    ui->actionConfigure->setEnabled(true);
    ui->statusBar->showMessage(tr("Disconnected"));
}


void MainWindow::about()
{
    QMessageBox::about(this, tr("About Simple Terminal"),
                       tr("The <b>Simple Terminal</b> example demonstrates how to "
                          "use the Qt Serial Port module in modern GUI applications "
                          "using Qt, with a menu bar, toolbars, and a status bar."));
}

void MainWindow::writeData(const QByteArray &data)
{
#ifdef SERIAL_ON
    serial->write(data);
#endif
#ifdef DEBUG
    QString str;
    ++iter;
    str.append("Send (" + QString::number(iter) + ") :");
    for(int i = 0; i < data.size(); i++)
        str += QString::number((u_int8_t) data[i], 16) + " ";
    qDebug() << str;
    //Debug Test
    debugArray = data;
    readData();
#endif
}

void MainWindow::readData()
{
#ifdef SERIAL_ON
    protocolMusv.SetData(serial->readAll());
#endif
#ifdef DEBUG
    protocolMusv.SetData(debugArray);
#endif
    protocolMusv.DecodingData();
    UpdateLayble();
}

void MainWindow::UpdateLayble()
{
    if(protocolMusv.GetData()->newAngles)
    {
        //Input Angels
        ui->lblRollIn->setText(QString::number(protocolMusv.GetData()->angles.roll, 'f', 2));
        ui->lblPitchIn->setText(QString::number(protocolMusv.GetData()->angles.pitch, 'f', 2));
        ui->lblYawIn->setText(QString::number(protocolMusv.GetData()->angles.yaw, 'f', 2));

        //Angels Error
        ui->lblRollError->setText(QString::number((double) (protocolMusv.GetData()->angles.roll - ui->sldRollOut->value()),
                                               'f', 2));
        ui->lblPitchError->setText(QString::number((double) (protocolMusv.GetData()->angles.pitch - ui->sldPitchOut->value()),
                                                'f', 2));
        ui->lblYawError->setText(QString::number((double) (protocolMusv.GetData()->angles.yaw - ui->sldYawOut->value()),
                                              'f', 2));

//        qDebug() << "ANGLES: " +
//                    QString::number(protocolMusv.GetData()->angles.roll) + " " +
//                    QString::number(protocolMusv.GetData()->angles.pitch) + " " +
//                    QString::number(protocolMusv.GetData()->angles.yaw);
    }

    //PID roll
    if(protocolMusv.GetData()->newPid.roll)
    {
        protocolMusv.GetData()->newPid.roll = false;
        if(protocolMusv.GetData()->pidFlag.roll.Kp)
            ui->txtRollKp->setText(QString::number(protocolMusv.GetData()->pid.roll.Kp));
        if(protocolMusv.GetData()->pidFlag.roll.Lp)
            ui->txtRollLp->setText(QString::number(protocolMusv.GetData()->pid.roll.Lp));
        if(protocolMusv.GetData()->pidFlag.roll.Ki)
            ui->txtRollKi->setText(QString::number(protocolMusv.GetData()->pid.roll.Ki));
        if(protocolMusv.GetData()->pidFlag.roll.Li)
            ui->txtRollLi->setText(QString::number(protocolMusv.GetData()->pid.roll.Li));
        if(protocolMusv.GetData()->pidFlag.roll.Kd)
            ui->txtRollKd->setText(QString::number(protocolMusv.GetData()->pid.roll.Kd));
        if(protocolMusv.GetData()->pidFlag.roll.Ld)
            ui->txtRollLd->setText(QString::number(protocolMusv.GetData()->pid.roll.Ld));
        if(protocolMusv.GetData()->pidFlag.roll.L)
            ui->txtRollL->setText(QString::number(protocolMusv.GetData()->pid.roll.L));
        protocolMusv.GetData()->pidFlag.roll.Kp = false;
        protocolMusv.GetData()->pidFlag.roll.Lp = false;
        protocolMusv.GetData()->pidFlag.roll.Ki = false;
        protocolMusv.GetData()->pidFlag.roll.Li = false;
        protocolMusv.GetData()->pidFlag.roll.Kd = false;
        protocolMusv.GetData()->pidFlag.roll.Ld = false;
        protocolMusv.GetData()->pidFlag.roll.L = false;
    }

    //PID pitch
    if(protocolMusv.GetData()->newPid.pitch)
    {
        protocolMusv.GetData()->newPid.pitch = false;
        if(protocolMusv.GetData()->pidFlag.pitch.Kp)
            ui->txtPitchKp->setText(QString::number(protocolMusv.GetData()->pid.pitch.Kp));
        if(protocolMusv.GetData()->pidFlag.pitch.Lp)
            ui->txtPitchLp->setText(QString::number(protocolMusv.GetData()->pid.pitch.Lp));
        if(protocolMusv.GetData()->pidFlag.pitch.Ki)
            ui->txtPitchKi->setText(QString::number(protocolMusv.GetData()->pid.pitch.Ki));
        if(protocolMusv.GetData()->pidFlag.pitch.Li)
            ui->txtPitchLi->setText(QString::number(protocolMusv.GetData()->pid.pitch.Li));
        if(protocolMusv.GetData()->pidFlag.pitch.Kd)
            ui->txtPitchKd->setText(QString::number(protocolMusv.GetData()->pid.pitch.Kd));
        if(protocolMusv.GetData()->pidFlag.pitch.Ld)
            ui->txtPitchLd->setText(QString::number(protocolMusv.GetData()->pid.pitch.Ld));
        if(protocolMusv.GetData()->pidFlag.pitch.L)
            ui->txtPitchL->setText(QString::number(protocolMusv.GetData()->pid.pitch.L));
        protocolMusv.GetData()->pidFlag.pitch.Kp = false;
        protocolMusv.GetData()->pidFlag.pitch.Lp = false;
        protocolMusv.GetData()->pidFlag.pitch.Ki = false;
        protocolMusv.GetData()->pidFlag.pitch.Li = false;
        protocolMusv.GetData()->pidFlag.pitch.Kd = false;
        protocolMusv.GetData()->pidFlag.pitch.Ld = false;
        protocolMusv.GetData()->pidFlag.pitch.L = false;
    }
    //PID yaw
    if(protocolMusv.GetData()->newPid.yaw)
    {
        protocolMusv.GetData()->newPid.yaw = false;
        if(protocolMusv.GetData()->pidFlag.yaw.Kp)
            ui->txtYawKp->setText(QString::number(protocolMusv.GetData()->pid.yaw.Kp));
        if(protocolMusv.GetData()->pidFlag.yaw.Lp)
            ui->txtYawLp->setText(QString::number(protocolMusv.GetData()->pid.yaw.Lp));
        if(protocolMusv.GetData()->pidFlag.yaw.Ki)
            ui->txtYawKi->setText(QString::number(protocolMusv.GetData()->pid.yaw.Ki));
        if(protocolMusv.GetData()->pidFlag.yaw.Li)
            ui->txtYawLi->setText(QString::number(protocolMusv.GetData()->pid.yaw.Li));
        if(protocolMusv.GetData()->pidFlag.yaw.Kd)
            ui->txtYawKd->setText(QString::number(protocolMusv.GetData()->pid.yaw.Kd));
        if(protocolMusv.GetData()->pidFlag.yaw.Ld)
            ui->txtYawLd->setText(QString::number(protocolMusv.GetData()->pid.yaw.Ld));
        if(protocolMusv.GetData()->pidFlag.yaw.L)
            ui->txtYawL->setText(QString::number(protocolMusv.GetData()->pid.yaw.L));
        protocolMusv.GetData()->pidFlag.yaw.Kp = false;
        protocolMusv.GetData()->pidFlag.yaw.Lp = false;
        protocolMusv.GetData()->pidFlag.yaw.Ki = false;
        protocolMusv.GetData()->pidFlag.yaw.Li = false;
        protocolMusv.GetData()->pidFlag.yaw.Kd = false;
        protocolMusv.GetData()->pidFlag.yaw.Ld = false;
        protocolMusv.GetData()->pidFlag.yaw.L = false;
    }

    if(protocolMusv.GetData()->newCalib)
    {
        protocolMusv.GetData()->newCalib = false;
        ui->lblMagCalibBiasX->setText(QString::number(protocolMusv.GetData()->bias.X, 'f', 5));
        ui->lblMagCalibBiasY->setText(QString::number(protocolMusv.GetData()->bias.Y, 'f', 5));
        ui->lblMagCalibBiasZ->setText(QString::number(protocolMusv.GetData()->bias.Z, 'f', 5));
        ui->lblMagCalibScaleX->setText(QString::number(protocolMusv.GetData()->scale.X, 'f', 5));
        ui->lblMagCalibScaleY->setText(QString::number(protocolMusv.GetData()->scale.Y, 'f', 5));
        ui->lblMagCalibScaleZ->setText(QString::number(protocolMusv.GetData()->scale.Z, 'f', 5));
    }
}

void MainWindow::initActionsConnections()
{
#ifdef SERIAL_ON
    connect(ui->actionConnect, SIGNAL(triggered()), this, SLOT(openSerialPort()));
    connect(ui->actionDisconnect, SIGNAL(triggered()), this, SLOT(closeSerialPort()));
//    connect(ui->actionClear, SIGNAL(triggered()), console, SLOT(clear()));
//    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(about()));
//    connect(ui->actionAboutQt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
//    connect(ui->actionAcceptCalib, SIGNAL(triggered()), this, SLOT(acceptCalibrationData()));
//    connect(ui->actionResetCalib, SIGNAL(triggered()), this, SLOT(resetCalibrationData()));
#endif
    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->actionConfigure, SIGNAL(triggered()), settings, SLOT(show()));
    connect(ui->actionClear, SIGNAL(triggered()), this, SLOT(resetLable()));
}

//Обработчики нажатия кнопок PID Roll
void MainWindow::on_btnRollKpSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_ROLL_Kp, ui->txtRollKp->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Kp", "Не верный формат данных");
}

void MainWindow::on_btnRollLpSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_ROLL_Lp, ui->txtRollLp->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Lp", "Не верный формат данных");
}

void MainWindow::on_btnRollKiSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_ROLL_Ki, ui->txtRollKi->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Ki", "Не верный формат данных");
}

void MainWindow::on_btnRollLiSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_ROLL_Li, ui->txtRollLi->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Li", "Не верный формат данных");
}

void MainWindow::on_btnRollKdSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_ROLL_Kd, ui->txtRollKd->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Kd", "Не верный формат данных");
}

void MainWindow::on_btnRollLdSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_ROLL_Ld, ui->txtRollLd->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Ld", "Не верный формат данных");
}

void MainWindow::on_btnRollLSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_ROLL_L, ui->txtRollL->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка L", "Не верный формат данных");
}

void MainWindow::on_btnSetPIDRoll_clicked()
{
    on_btnRollKpSet_clicked();
    on_btnRollLpSet_clicked();
    on_btnRollKiSet_clicked();
    on_btnRollLiSet_clicked();
    on_btnRollKdSet_clicked();
    on_btnRollLdSet_clicked();
    on_btnRollLSet_clicked();
}

void MainWindow::on_btnReadPIDRoll_clicked()
{
    protocolMusv.GetData()->newPid.roll = false;
    protocolMusv.GetData()->pidFlag.roll.Kp = false;
    protocolMusv.GetData()->pidFlag.roll.Lp = false;
    protocolMusv.GetData()->pidFlag.roll.Ki = false;
    protocolMusv.GetData()->pidFlag.roll.Li = false;
    protocolMusv.GetData()->pidFlag.roll.Kd = false;
    protocolMusv.GetData()->pidFlag.roll.Ld = false;
    protocolMusv.GetData()->pidFlag.roll.L = false;
    ui->txtRollKp->setText("");
    ui->txtRollLp->setText("");
    ui->txtRollKi->setText("");
    ui->txtRollLi->setText("");
    ui->txtRollKd->setText("");
    ui->txtRollLd->setText("");
    ui->txtRollL->setText("");
    writeData(protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_ROLL_REQ));
}

//Обработчики нажатия кнопок PID Pitch
void MainWindow::on_btnPitchKpSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_PITCH_Kp, ui->txtPitchKp->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Kp", "Не верный формат данных");
}

void MainWindow::on_btnPitchLpSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_PITCH_Lp, ui->txtPitchLp->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Lp", "Не верный формат данных");
}

void MainWindow::on_btnPitchKiSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_PITCH_Ki, ui->txtPitchKi->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Ki", "Не верный формат данных");
}

void MainWindow::on_btnPitchLiSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_PITCH_Li, ui->txtPitchLi->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Li", "Не верный формат данных");
}

void MainWindow::on_btnPitchKdSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_PITCH_Kd, ui->txtPitchKd->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Kd", "Не верный формат данных");
}

void MainWindow::on_btnPitchLdSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_PITCH_Ld, ui->txtPitchLd->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Ld", "Не верный формат данных");
}

void MainWindow::on_btnPitchLSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_PITCH_L, ui->txtPitchL->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка L", "Не верный формат данных");
}

void MainWindow::on_btnSetPIDPitch_clicked()
{
    on_btnPitchKpSet_clicked();
    on_btnPitchLpSet_clicked();
    on_btnPitchKiSet_clicked();
    on_btnPitchLiSet_clicked();
    on_btnPitchKdSet_clicked();
    on_btnPitchLdSet_clicked();
    on_btnPitchLSet_clicked();
}

void MainWindow::on_btnReadPIDPitch_clicked()
{
    protocolMusv.GetData()->newPid.pitch = false;
    protocolMusv.GetData()->pidFlag.pitch.Kp = false;
    protocolMusv.GetData()->pidFlag.pitch.Lp = false;
    protocolMusv.GetData()->pidFlag.pitch.Ki = false;
    protocolMusv.GetData()->pidFlag.pitch.Li = false;
    protocolMusv.GetData()->pidFlag.pitch.Kd = false;
    protocolMusv.GetData()->pidFlag.pitch.Ld = false;
    protocolMusv.GetData()->pidFlag.pitch.L = false;
    ui->txtPitchKp->setText("");
    ui->txtPitchLp->setText("");
    ui->txtPitchKi->setText("");
    ui->txtPitchLi->setText("");
    ui->txtPitchKd->setText("");
    ui->txtPitchLd->setText("");
    ui->txtPitchL->setText("");
    writeData(protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_PITCH_REQ));
}

//Обработка нажатия кнопок PID Yaw
void MainWindow::on_btnYawKpSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_YAW_Kp, ui->txtYawKp->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Kp", "Не верный формат данных");
}

void MainWindow::on_btnYawLpSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_YAW_Lp, ui->txtYawLp->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Lp", "Не верный формат данных");
}

void MainWindow::on_btnYawKiSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_YAW_Ki, ui->txtYawKi->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Ki", "Не верный формат данных");
}

void MainWindow::on_btnYawLiSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_YAW_Li, ui->txtYawLi->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Li", "Не верный формат данных");
}

void MainWindow::on_btnYawKdSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_YAW_Kd, ui->txtYawKd->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Kd", "Не верный формат данных");
}

void MainWindow::on_btnYawLdSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_YAW_Ld, ui->txtYawLd->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка Ld", "Не верный формат данных");
}

void MainWindow::on_btnYawLSet_clicked()
{
    QByteArray data = protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_YAW_L, ui->txtYawL->text());
    if(data.size() > 0)
        writeData(data);
    else
        QMessageBox::critical(0, "Ошибка L", "Не верный формат данных");
}

void MainWindow::on_btnSetPIDYaw_clicked()
{
    on_btnYawKpSet_clicked();
    on_btnYawLpSet_clicked();
    on_btnYawKiSet_clicked();
    on_btnYawLiSet_clicked();
    on_btnYawKdSet_clicked();
    on_btnYawLdSet_clicked();
    on_btnYawLSet_clicked();
}

void MainWindow::on_btnReadPIDYaw_clicked()
{
    protocolMusv.GetData()->newPid.yaw = false;
    protocolMusv.GetData()->pidFlag.yaw.Kp = false;
    protocolMusv.GetData()->pidFlag.yaw.Lp = false;
    protocolMusv.GetData()->pidFlag.yaw.Ki = false;
    protocolMusv.GetData()->pidFlag.yaw.Li = false;
    protocolMusv.GetData()->pidFlag.yaw.Kd = false;
    protocolMusv.GetData()->pidFlag.yaw.Ld = false;
    protocolMusv.GetData()->pidFlag.yaw.L = false;
    ui->txtYawKp->setText("");
    ui->txtYawLp->setText("");
    ui->txtYawKi->setText("");
    ui->txtYawLi->setText("");
    ui->txtYawKd->setText("");
    ui->txtYawLd->setText("");
    ui->txtYawL->setText("");
    writeData(protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_PID_YAW_REQ));
}

//Обработка ползунков установки углового положения
void MainWindow::on_sldRollOut_valueChanged(int value)
{
    ui->lblRollOut->setText(QString::number(value));
    UpdateAngles();
}

void MainWindow::on_sldPitchOut_valueChanged(int value)
{
    ui->lblPitchOut->setText(QString::number(value));
    UpdateAngles();
}

void MainWindow::on_sldYawOut_valueChanged(int value)
{
    ui->lblYawOut->setText(QString::number(value));
    UpdateAngles();
}

void MainWindow::UpdateAngles()
{
    writeData(protocolMusv.GenerateAngles(ui->sldRollOut->value(),
                                          ui->sldPitchOut->value(),
                                          ui->sldYawOut->value()));
}

//Обработчика калибровки магнитометра
void MainWindow::ClearCalibData()
{
    ui->lblMagCalibBiasX->setText("not read");
    ui->lblMagCalibBiasY->setText("not read");
    ui->lblMagCalibBiasZ->setText("not read");
    ui->lblMagCalibScaleX->setText("not read");
    ui->lblMagCalibScaleY->setText("not read");
    ui->lblMagCalibScaleZ->setText("not read");
}

void MainWindow::on_btnMagCalibReadData_clicked()
{
    ClearCalibData();
    writeData(protocolMusv.GenerateMessage(MUSV_SERVICE, MUSV_SERVICE_CALIB_REQ));
}

void MainWindow::on_btnMagCalibOn_clicked()
{
    writeData(protocolMusv.GenerateMessage((u_int8_t)MUSV_SETTINGS,
                                           (u_int8_t)MUSV_SETTINGS_CALIBRATION,
                                           (u_int8_t) MUSV_SETTINGS_CALIBRATION_ON));
}

void MainWindow::on_btnMagCalibOff_clicked()
{
    writeData(protocolMusv.GenerateMessage((u_int8_t) MUSV_SETTINGS,
                                           (u_int8_t) MUSV_SETTINGS_CALIBRATION,
                                           (u_int8_t) MUSV_SETTINGS_CALIBRATION_OFF));
}

//Обработка кнопок
void MainWindow::SetZoom(u_int8_t value)
{
    if(value >= 30)
    {
        value = 30;
        ui->btnZoomInc->setEnabled(false);
    }
    else if(value <= 1)
    {
        value = 1;
        ui->btnZoomDec->setEnabled(false);
    }
    if(value > 1)
        ui->btnZoomDec->setEnabled(true);
    if(value < 30)
        ui->btnZoomInc->setEnabled(true);

    ui->lblZoom->setText(QString::number(value));
}

void MainWindow::on_btnZoomInc_clicked()
{
    SetZoom(++zoom);
    writeData(protocolMusv.GenerateMessage(MUSV_SETTINGS, MUSV_SETTINGS_ZOOM, zoom));
}

void MainWindow::on_btnZoomDec_clicked()
{
    SetZoom(--zoom);
    writeData(protocolMusv.GenerateMessage(MUSV_SETTINGS, MUSV_SETTINGS_ZOOM, zoom));
}

void MainWindow::SetColor(u_int8_t value)
{
    if(value >= 10)
    {
        value = 10;
        ui->btnColorInc->setEnabled(false);
    }
    else if(value <= 1)
    {
        value = 1;
        ui->btnColorDec->setEnabled(false);
    }
    if(value > 1)
        ui->btnColorDec->setEnabled(true);
    if(value < 10)
        ui->btnColorInc->setEnabled(true);

    ui->lblColor->setText(QString::number(value));
}

void MainWindow::on_btnColorInc_clicked()
{
    SetColor(++color);
    writeData(protocolMusv.GenerateMessage(MUSV_SETTINGS, MUSV_SETTINGS_COLOR, color));
}

void MainWindow::on_btnColorDec_clicked()
{
    SetColor(--color);
    writeData(protocolMusv.GenerateMessage(MUSV_SETTINGS, MUSV_SETTINGS_COLOR, color));
}

void MainWindow::on_btbCameraTV_clicked()
{
    writeData(protocolMusv.GenerateMessage((u_int8_t)MUSV_SETTINGS,
                                           (u_int8_t)MUSV_SETTINGS_CAMERA,
                                           (u_int8_t)MUSV_SETTINGS_CAMERA_TV));
}

void MainWindow::on_btbCameraIR_clicked()
{
    writeData(protocolMusv.GenerateMessage((u_int8_t)MUSV_SETTINGS,
                                           (u_int8_t)MUSV_SETTINGS_CAMERA,
                                           (u_int8_t)MUSV_SETTINGS_CAMERA_IR));
}

void MainWindow::on_btbCameraPhoto_clicked()
{
    writeData(protocolMusv.GenerateMessage((u_int8_t)MUSV_SETTINGS,
                                           (u_int8_t)MUSV_SETTINGS_CAMERA,
                                           (u_int8_t)MUSV_SETTINGS_CAMERA_PHOTO));
}


void MainWindow::on_btnFocus_clicked()
{
    writeData(protocolMusv.GenerateMessage((u_int8_t)MUSV_SETTINGS,
                                           (u_int8_t)MUSV_SETTINGS_FOCUS,
                                           (u_int8_t)MUSV_SETTINGS_FOCUS_ON));
}

void MainWindow::on_btnTakePhoto_clicked()
{
    writeData(protocolMusv.GenerateMessage(MUSV_SETTINGS,
                                           MUSV_SETTINGS_PHOTO,
                                           MUSV_SETTINGS_PHOTO_TAKE_PHOTO,
                                           0));
}

void MainWindow::on_btnBurstClear_clicked()
{
    ui->spbBurstTime->setValue(1.0);
}

void MainWindow::on_btnBurst_clicked()
{
    u_int16_t time = (u_int16_t) (ui->spbBurstTime->value() * 100);
    writeData(protocolMusv.GenerateMessage(MUSV_SETTINGS,
                                           MUSV_SETTINGS_PHOTO,
                                           MUSV_SETTINGS_PHOTO_BURST,
                                           time));
}

void MainWindow::on_btnBurstStop_clicked()
{
    writeData(protocolMusv.GenerateMessage(MUSV_SETTINGS,
                                           MUSV_SETTINGS_PHOTO,
                                           MUSV_SETTINGS_PHOTO_NO,
                                           0));
}


void MainWindow::on_btnStabilizationOn_clicked()
{
    writeData(protocolMusv.GenerateMessage((u_int8_t)MUSV_SETTINGS,
                                           (u_int8_t)MUSV_SETTINGS_STABILISATION,
                                           (u_int8_t)MUSV_SETTINGS_STABILISATION_ON));
}

void MainWindow::on_btnStabilizationOff_clicked()
{
    writeData(protocolMusv.GenerateMessage((u_int8_t)MUSV_SETTINGS,
                                           (u_int8_t)MUSV_SETTINGS_STABILISATION,
                                           (u_int8_t)MUSV_SETTINGS_STABILISATION_OFF));
}

void MainWindow::on_btnMotorOn_clicked()
{
    writeData(protocolMusv.GenerateMessage((u_int8_t)MUSV_SETTINGS,
                                           (u_int8_t)MUSV_SETTINGS_MOTOR,
                                           (u_int8_t)MUSV_SETTINGS_MOTOR_ON));
}

void MainWindow::on_btnMotorOff_clicked()
{
    writeData(protocolMusv.GenerateMessage((u_int8_t)MUSV_SETTINGS,
                                           (u_int8_t)MUSV_SETTINGS_MOTOR,
                                           (u_int8_t)MUSV_SETTINGS_MOTOR_OFF));
}

void MainWindow::on_btnParkingOn_clicked()
{
    ui->sldRollOut->setValue(-170);
    ui->gbxAnglesOut->setEnabled(false);
    ui->lblParkingName->setVisible(true);
    ui->btnParkingOn->setEnabled(false);
    ui->btnParkingOff->setEnabled(true);
}

void MainWindow::on_btnParkingOff_clicked()
{
    ui->sldRollOut->setValue(0);
    ui->gbxAnglesOut->setEnabled(true);
    ui->lblParkingName->setVisible(false);
    ui->btnParkingOn->setEnabled(true);
    ui->btnParkingOff->setEnabled(false);
}

void MainWindow::on_btnSetZero_clicked()
{
    writeData(protocolMusv.GenerateMessage(MUSV_SENSOR_ZERO));
}

