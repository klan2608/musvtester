#-------------------------------------------------
#
# Project created by QtCreator 2015-06-11T20:07:34
#
#-------------------------------------------------

QT       += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4) {
    QT       += serialport
} else {
    include($$QTSERIALPORT_PROJECT_ROOT/src/serialport/qt4support/serialport.prf)
}

TARGET = MUSV_Tester
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    settingsdialog.cpp \
    protocol.cpp

HEADERS  += mainwindow.h \
    settingsdialog.h \
    protocol.h

FORMS    += mainwindow.ui \
    settingsdialog.ui

RESOURCES += \
    image.qrc

DEFINES += SERIAL_ON
#DEFINES += DEBUG
